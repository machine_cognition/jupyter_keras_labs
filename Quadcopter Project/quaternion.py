import numpy as np

class Quaternion:
    """Quaternion class"""
    def __init__(self, quat = (1.0, 0.0, 0.0, 0.0)):
        # check if we have passed a Quaternion instance
        if isinstance(quat, Quaternion):
            quat = quat.q
            self._q = quat
        else:
            self.q = quat
                        
    @property
    def q(self):
        return self._q

    @q.setter
    def q(self, quat):
        quat = np.array(quat)
        quat_len = len(quat)
        # perform error checking
        if quat_len < 3 or quat_len > 4:
            raise TypeError("Quaternion can be initialized with size (1, 3) for euler angle, or (1, 4) array or with quaternion object!")
        
        if quat_len == 3:
            quat = self._eul2quat(quat)
            
        if abs(np.sum(quat**2) - 1.0) > 1e-6:
            raise ValueError('Quaternion must be normalized!')
        self._q = quat
    
    def __mul__(self, quat):
        """Miltiply quaternions."""
        p = self.q
        q = quat.q
        Q = np.array([[p[0], -p[1], -p[2], -p[3]],
                      [p[1],  p[0], -p[3],  p[2]],
                      [p[2],  p[3],  p[0], -p[1]],
                      [p[3], -p[2],  p[1],  p[0]]])
        return Quaternion(np.dot(Q, q.T))
    
    
    def __div__(self, quat):
        """Divide quaternions."""
        return self * quat.inv()
    
    def _eul2quat(self, angles):
        # Unpack angles    
        c = np.cos(angles/2)
        s = np.sin(angles/2)

        q = np.array([c[0]*c[1]*c[2]+s[0]*s[1]*s[2],
                      s[0]*c[1]*c[2]-c[0]*s[1]*s[2], 
                      c[0]*s[1]*c[2]+s[0]*c[1]*s[2], 
                      c[0]*c[1]*s[2]-s[0]*s[1]*c[2]])
        return q
    
    def inv(self):
        """Quaternion inverse."""
        q = self.q
        return Quaternion((q[0], -q[1], -q[2], -q[3]))
                          
    def derivative(self, omega):
        p = np.hstack((0.0, omega))
        Q = np.array([[p[0], -p[1], -p[2], -p[3]],
                      [p[1],  p[0], -p[3],  p[2]],
                      [p[2],  p[3],  p[0], -p[1]],
                      [p[3], -p[2],  p[1],  p[0]]])
        qdot = -0.5*np.dot(Q, self.q.T) # right-hand quaternion derivative, 
                                        # i.e. counterclockwise rotation is considered to be positive            
        return qdot
    # Conversion from Quaternion to Rotation Matrix (ZYX)
    def rotm(self):
        """Convert quaternion to rotation matrix."""
        q0square = self._q[0]**2
        q1square = self._q[1]**2
        q2square = self._q[2]**2
        q3square = self._q[3]**2
        q1q2 = self._q[1]*self._q[2]
        q0q3 = self._q[0]*self._q[3]
        q1q3 = self._q[1]*self._q[3]
        q0q2 = self._q[0]*self._q[2]
        q2q3 = self._q[2]*self._q[3]
        q0q1 = self._q[0]*self._q[1]

        R = np.array([[1-2*(q2square+q3square), 2*(q1q2-q0q3), 2*(q0q2+q1q3)], 
                      [2*(q1q2+q0q3), 1-2*(q1square+q3square), 2*(q2q3-q0q1)], 
                      [2*(q1q3-q0q2), 2*(q0q1+q2q3), 1-2*(q1square+q2square)]])
        return R
    
    # Conversion from Quaternion to Euler Angles (ZYX)
    def eul(self):
        """Convert quaternion to Euler angle."""
        # Pre-compute repeating operations
        q2square = self._q[2]**2;

        arcsinInput = np.clip(2*(self._q[0]*self._q[2]-self._q[3]*self._q[1]), -1, 1);  
                                        # bound inputs to asin between (-1, 1), since values > 1 produce complex results
                                        # Since the quaternion is of unit length, this should never happen, however due to 
                                        # numerical imprecision, this can happen
        angles = np.array([np.arctan2(2*(self._q[0]*self._q[1]+self._q[2]*self._q[3]), 1-2*(self._q[1]**2+q2square)), 
                           np.arcsin(arcsinInput), 
                           np.arctan2(2*(self._q[0]*self._q[3]+self._q[1]*self._q[2]), 1-2*(q2square+self._q[3]**2))])

        return angles

def normalize(array):
    quat = np.array(array)
    return quat / np.sqrt(np.dot(quat, quat))