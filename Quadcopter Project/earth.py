class Earth:
    """"Holds Earth constants."""
    GRAVITY = 9.81                   # [m/s**2]
    ATMOSPHERIC_PRESSURE = 101325    # [Pa]